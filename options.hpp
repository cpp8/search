#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_
#include <string>

using namespace std ;

class Options {
    public:
        Options(int argc, char **argv) ;
        bool verbose;
        bool recursive;
        bool regex;
        string candidate;
        string recursdir;
        void Help() ;
        void Show() ;
        int ArgPtr() ;

    private:
        int argptr ;
        int argc;
        char **argv;
        Options() ;
} ;

#endif