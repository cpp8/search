#ifndef _SEARCHFILE_HPP_
#define  _SEARCHFILE_HPP_
#include <string>
#include <regex>
using namespace std ;
class Search {
    public:
        Search(string _candidate) ;
        void File(string _filename) ;
        void Files(string _files, string _names) ;
        void Summarize() ;

        bool virtual Find(string _line) = 0;
    protected:
        string candidate;
    private:
        int files_searched ;
        int occurrences ;
} ;

class StringSearch : public Search {
    public:
        StringSearch(string _candidate) ;
        bool Find(string _line) ;
} ;

class RegexSearch : public Search {
    public:
        RegexSearch(string _candidate);
        bool Find(string _line) ;
    private:
        regex *candexp ;
} ;

#endif
