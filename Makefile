CXX=clang
EXEC := search
SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp) 
OBJECTS = $(SOURCES:.cpp=.o)

-include $(OBJS:%.o=%.d)
CXXFLAGS := -g -std=gnu++17 -MMD
LDFLAGS := -lstdc++ -lstdc++fs
all: $(OBJECTS) $(HEADERS)
	$(CXX) $(OBJECTS) $(CXXFLAGS) $(LDFLAGS) -o $(EXEC)

clean:
	ls $(SOURCES)
	ls $(OBJECTS)
	rm -f $(EXEC) $(OBJECTS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $(<)