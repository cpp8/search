#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#include <fnmatch.h>

#include "searchfile.hpp"

using namespace std ;

Search::Search(string _candidate) 
{
    files_searched = 0 ;
    occurrences = 0 ;
    candidate = _candidate ;
    cerr << "Search candidate base class is " << _candidate << endl ;
}

void Search::File(string _filename) {
    cerr << "Search file " << _filename << " for " << candidate << endl ;

    if (!filesystem::is_regular_file(_filename)) {
        cerr << _filename << " is not a regular file" << endl ;
        return ;
    }

    ifstream ifile(_filename, ios::binary) ;
    if (!ifile.is_open()) {
        cerr << "Error opening " << _filename << endl ;
        return ;
    }

    int linenum = 0 ;
    string iline ;
    int thisfileocc = 0 ;
    while (!ifile.eof()) {   
        getline(ifile,iline);
        linenum++ ;
        if (Find(iline)) {
            cout << setw(4) << setfill('0') << linenum << " : " << iline << endl ;
            occurrences++ ;
            thisfileocc++ ;
        }
    }
    ifile.close() ;
    files_searched++ ;
    if (thisfileocc > 0)
    {
        cout << thisfileocc << " occurrences" << endl ;
    }
    else
    {
        cout << "Not found" << endl ;
    }
    
}

void Search::Files(string _files, string _names) {

    cerr << "Search files of " << _files << endl ;
    if (filesystem::is_regular_file(_files)) {
        File(_files) ;
        return ;
    }

    for (const auto& entry : filesystem::recursive_directory_iterator(_files, filesystem::directory_options::skip_permission_denied) ) {
        const auto filenameStr = entry.path().filename().string();
        if (entry.is_regular_file()) {
            if (fnmatch(_names.c_str(),filenameStr.c_str(),FNM_FILE_NAME)) {
                cerr << "file: " << filenameStr << '\n';
                File(entry.path()) ;
            }
        }
        else
            cerr << "??    " << entry.path() << '\n';
    }

}

void Search::Summarize() {
    if (files_searched > 0)
    {
        cout << "Files Searched : " << files_searched << endl ;
        cout << "Total Number of occurrences " << occurrences << endl ;
    }
}

StringSearch::StringSearch(string _candidate) 
: Search(_candidate)
{
    cerr << "StringSearch: Search candidate is " << _candidate << endl ;
    cerr << "Now " << candidate << " replacing with " << _candidate << endl ;
    candidate = _candidate ;
}
bool StringSearch::Find(string _line) {
    int pos = _line.find(candidate) ;
    if (pos != string::npos) {
        return true ;
    }
    return false ;
}


RegexSearch::RegexSearch(string _candidate) 
: Search(_candidate)
{
    cerr << "RegexSearch: Search candidate is " << _candidate << endl ;
    regex_constants::syntax_option_type regexopt = regex::grep ;
    candexp = new regex( candidate );
}

bool RegexSearch::Find(string _line) {
    bool matched = regex_search( _line.c_str() , *candexp );
    return matched ;
}

