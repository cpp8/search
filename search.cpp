#include <iostream>
#include "options.hpp"
#include "searchfile.hpp"
using namespace std ;

int main(int argc, char **argv) {
    Options *options = new Options(argc,argv) ;
    Search *search = NULL ;
    int arg = options->ArgPtr() ;

    if (options->regex) {
        search = new RegexSearch( options->candidate ); 
    } else {
        search =  new StringSearch( options->candidate );
    }

    if (options->recursive) {
        if (arg >= argc) {
            cerr << "For recursive search, please provide a filename pattern to match" << endl ;
            exit(EXIT_FAILURE);
        }
        search->Files( options->recursdir , argv[arg] ) ;
    } else {
        for (; arg < argc ; arg++ ) {
            search->File( argv[arg] );
        }
    }

    search->Summarize() ;
}