# search
```
search - Version 0.1 - Mon Aug 17 10:33:54 2020
usage: search options arg1 arg2 arg3
         -v      Verbose
         -h      Help
         -r      Recursive search. If arg is dir, will recurse to lower levels
         -s      String. use the string as the search argument
         -p      Pattern. Use the argument as a regex pattern to search
```

# examples
## Simple string search
```
$ ./search -s include *.hpp
Search file options.hpp for include
0003 : #include <string>
1 occurrences
Search file searchfile.hpp for include
0003 : #include <string>
0004 : #include <regex>
2 occurrences
Files Searched : 2
Total Number of occurrences 3
```
## Regex pattern search
```
$ ./search -p "\<.*\.h\>" *.*pp
Search file options.cpp for \<.*\.h\>
0002 : #include <unistd.h>
1 occurrences
Search file options.hpp for \<.*\.h\>
Not found
Search file search.cpp for \<.*\.h\>
Not found
Search file searchfile.cpp for \<.*\.h\>
Not found
Search file searchfile.hpp for \<.*\.h\>
Not found
Files Searched : 5
Total Number of occurrences 1
```
## Recursive search
```
$ ./search -s include -r ./test/ *.*pp
Search candidate base class is include
StringSearch: Search candidate is include
Now include replacing with include
Search files of ./test/
??    "./test/hello"
file: hello.cpp
Search file ./test/hello/hello.cpp for include
0001 : #include <iostream>
0002 : #include "lister.hpp"
2 occurrences
file: lister.cpp
Search file ./test/hello/lister.cpp for include
0001 : #include <iostream>
0002 : #include <fstream>
0003 : #include <string>
0004 : #include <iomanip>
0005 : #include <filesystem>
5 occurrences
file: lister.hpp
Search file ./test/hello/lister.hpp for include
Not found
file: options.hpp
Search file ./test/options.hpp for include
0003 : #include <string>
1 occurrences
file: searchfile.hpp
Search file ./test/searchfile.hpp for include
0003 : #include <string>
0004 : #include <regex>
2 occurrences
Files Searched : 5
Total Number of occurrences 10
```