#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include "options.hpp"

using namespace std ;

Options::Options(int _argc, char **_argv) 
: argc(_argc) , argv(_argv) , argptr(0)
{
    if (argc <= 1) {
        Help() ;
        exit(EXIT_FAILURE) ;
    }
    verbose = false ;
    recursive = false ;
    regex = false ;
    candidate = "" ;
    ArgPtr() ;
}

void Options::Help() {
    cerr << "search - Version 0.1 - " << __TIMESTAMP__ << endl ;
    cerr << "usage: search options arg1 arg2 arg3" << endl ;
    cerr << "\t -v \t Verbose" << endl ;
    cerr << "\t -h \t Help" << endl ;
    cerr << "\t -r <dirname>\t Recursive search. dirname is a dir, will recurse to lower levels" << endl ;
    cerr << "\t     with the recursive option, the arguments are listof patterns to search for" << endl ;
    cerr << "\t -s \t <string>. use the string as the search argument" << endl ;
    cerr << "\t -p \t <pattern>. Use the pattern as a regex pattern to search" << endl ;
    exit(EXIT_FAILURE);
}

void Options::Show() {
    cerr << "Options playback " << endl ;
    cerr << "Recursive " << recursive << endl ;
    cerr << "Regex " << regex << endl ;
    cerr << "Candidate " << candidate << endl ;
}

int Options::ArgPtr() {
    int opt ;

    if (optind > 1) {
        return optind ;
    }

    while ((opt = getopt(argc, argv, "hp:r:s:v")) != -1) {
        switch (opt) {
        case 'h':
            Help() ;
            break ;
        case 'v':
            verbose = true ;
            cerr << "Verbose" << endl ;
            continue ;
        case 'p':
            if (candidate.length() > 0) {
                cerr << "Please use either a string or a pattern as the argument" << endl ;
                exit(EXIT_FAILURE);
            }
            candidate = optarg ;
            regex = true ;
            continue ;
        case 's':
            if (candidate.length() > 0) {
                cerr << "Please use either a string or a pattern as the argument" << endl ;
                exit(EXIT_FAILURE);
            }
            candidate = optarg ;
            if (verbose) {
                cerr << "Search string from cli " << optarg << " setting to " << candidate << endl ;
            }
            regex = false ;
            continue ;
        case 'r':
            recursive = true ;
            recursdir = optarg ;
            continue ;
        default: 
            Help();
            break;
        }
    }

    if (verbose) {
        Show() ;
    }

    if (candidate.length() < 1) {
        cerr << "Please provide a search candidate" << endl ;
        exit(EXIT_FAILURE) ;
    }
    return optind ;    
}